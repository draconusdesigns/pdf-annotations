<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jobs_model extends CI_Model {

	public function __construct() {
		
	}
	public function getJobs() {
		$this->db->join('statuses', 'statuses.status_id = jobs.status_id');
		$query = $this->db->get("jobs");
		return $query->result();
	}

	public function addJob($data) {	
		$query = array(
			'company' => $this->input->post('company'),
		);
				
		$this->db->insert('jobs', $query);
		
		$id = $this->db->insert_id();		
	}
	
	public function getJob($job_id) {
		$query = $this->db->get_where('jobs', array('job_id' => $job_id));		
		return $query->row();
	}
	
	public function updateJob($data) {
		$query = array(
			'id' => $this->input->post('id'),
		);
		
		$this->db->where('id', $query['id']);
		$this->db->update('job', $query);					
	}
	
	public function getStatusDropdown() {
		$query = $this->db->get('statuses');		
		
		foreach ($query->result_array() as $row){
			$data[$row['status_id']] = $row['status'];
		}
		
		return $data;
	}
	
	public function getTypeDropdown() {
		$query = $this->db->get('job_types');		
		
		foreach ($query->result_array() as $row){
			$data[$row['job_type_id']] = $row['job_type'];
		}
		
		return $data;
	}
	
	public function getRegionsDropdown() {
		$query = $this->db->get('marketing_regions');		
		
		foreach ($query->result_array() as $row){
			$data[$row['marketing_region_id']] = $row['marketing_region'];
		}
		
		return $data;
	}
	
	public function getBuDropdown() {
		$query = $this->db->get('business_units');		
		
		foreach ($query->result_array() as $row){
			$data[$row['business_unit_id']] = $row['business_unit'];
		}
		
		return $data;
	}
	
	public function getCategoryDropdown() {
		$query = $this->db->get('categories');		
		
		foreach ($query->result_array() as $row){
			$data[$row['category_id']] = $row['category'];
		}
		
		return $data;
	}
	
	public function getTierDropdown() {
		$query = $this->db->get('tiers');		
		
		foreach ($query->result_array() as $row){
			$data[$row['tier_id']] = $row['tier'];
		}
		
		return $data;
	}

}