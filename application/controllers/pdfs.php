<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdfs extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		//$this->load->model('jobs_model');

		$this->output->enable_profiler(TRUE);
		$this->load->helper('directory');
	}
	
	public function index() {
		$data['titleTag'] = "PDF Files";
		$data['pageHeading'] = "PDF Files";
		
		$files = directory_map('./uploads/pdf/', 1);
		$new_files = array();
		foreach ( $files as $file ) {
		  $fX = explode('.', $file);
		  $doc = new Imagick(upload_path().'/pdf/'.$file);
		  $method = !is_dir(upload_path() . '/' . $fX[0]) ? 'convert' : 'view';
		  $count = $method == 'view' ? count(directory_map(upload_path() . '/' . $fX[0])) : $doc->getnumberimages();
		
		  $new_files[] = [
		    'file' => $file,
		    'count' => $count,
		    'name' => $image = basename($file, ".pdf"),
		    'method' => $method
		  ];
		}
		sort($new_files);
		$data['files'] = $this->obj($new_files);
				
		$this->load->view('templates/header', $data);
		$this->load->view('index', $data);
		$this->load->view('templates/footer', $data);	
	}
	
	public function gallery($file) {
		$data['titleTag'] = "Annotation Gallery";
		$data['pageHeading'] = "Annotation Gallery for {$file}.pdf";
		
		$images = directory_map('./uploads/'.$file.'/', 1);
		$data['img_arr'] = $images;
		if(is_array($images)):
			$new_images = array();
			
			foreach($images as $image):
				$new_images[] = base_url().'uploads/'.$file.'/'.$image;
			endforeach;
			sort($new_images);
			$images = $new_images;
		else:
			$images = base_url().'uploads/'.$file.'/'.$images;
		endif;
		$data['images'] = $images;	
					
		$this->load->view('templates/header', $data);
		$this->load->view('gallery', $data);
		$this->load->view('templates/footer', $data);	
	}
	
	public function upload() {
		$config['upload_path'] = './uploads/pdf'; 
		$config['allowed_types'] = 'pdf'; 
		$config['max_size']	= 0; 
		$config['overwrite'] = TRUE; 
		
		
		// load upload library with custom config settings 
		$this->load->library('upload', $config);
		
		$data['titleTag'] = "Upload PDF";
		$data['pageHeading'] = "Upload a PDF";
		
		if($this->upload->do_upload()):
			$this->session->set_flashdata('success','file upload success');	
			redirect(base_url(), 'location');		
		else:
			$this->session->set_flashdata('error','file upload failed');			
			$this->load->view('templates/header', $data);
			$this->load->view('upload', $data);
			$this->load->view('templates/footer', $data);
		endif;		
	}
	
	public function convert($file = null){
		$file || redirect(base_url(), 'location');		
		
		mkdir(upload_path()."/".$file, 0777);
		
		$imagick = new imagick();
		$imagick->readImage(upload_path().'/pdf/'.$file.'.pdf'); 
		$imagick->setResolution(144, 144);
		$imagick->setImageFormat('jpeg');    		
		$imagick->writeImages(upload_path().'/'.$file.'/'.$file.'.jpg', false); 
		$imagick->clear(); 
		$imagick->destroy();
		
		redirect(base_url(), 'location');		
	}

    public function save_comments() {
        $sdata = $this->input->post('sdata');
        $ret_array = array();
        foreach ( $sdata as $data ) {
            $ret_array[] = $data;
        }
        $this->json($ret_array);
    }

}