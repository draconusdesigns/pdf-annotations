<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
    'job_validation' => array(
		array(
			'field' => 'job_name',
			'label' => 'Job Name',
			'rules' => 'trim|xss_clean'
		)
    )
);
