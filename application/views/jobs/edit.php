<?php if(validation_errors()):?>
	<div class='alert alert-error span4'><?= validation_errors() ?></div>
<?php endif;?>

<form class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="job_id" value="<?= set_value('job_id', $job->job_id) ?>" />
				
	<div class="form-group">
		<label for="job_name" class="col-md-3 control-label">Job Name: </label>
		<div class="col-md-5">
			<input type="text" name="job_name" class="input-xxlarge form-control" value="<?= set_value('job_name', $job->job_name) ?>"/>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="job_status" class="col-md-3 control-label">Job Status: </label>
		<div class="col-md-5">
			<?= form_dropdown('job_status', $statuses) ?>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="user_id" class="col-md-3 control-label">Job Owner: </label>	
		<div class="col-md-5">
			<?= form_dropdown('job_status', $users) ?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="job_type" class="col-md-3 control-label">Job Type: </label>
		<label for="job_type" class="col-md-3 control-label">Job Type: </label>
				<div class="col-md-5">
			<?= form_dropdown('job_type_id', $jobTypes) ?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="marketing_region" class="col-md-3 control-label">Market Region: </label>
	</div>
	
	<div class="form-group">
		<label for="business_unit_id" class="col-md-3 control-label">Business Unit: </label>
	</div>
	
	<div class="form-group">
		<label for="category_id" class="col-md-3 control-label">Category: </label>
	</div>
	
	<div class="form-group">
		<label for="tier_id" class="col-md-3 control-label">Tier: </label>
	</div>
	
	<div class="form-group">
		<label for="mp_display" class="col-md-3 control-label">Display on Marketing Portal?: </label>
		<div class="checkbox col-md-5">
			<input type="checkbox" name="mp_display" value="1" <?= set_checkbox('mp_display',$job->mp_display) ?> />
		</div>
	</div>
	
	<div class="form-group">
		<label for="mp_description" class="col-md-3 control-label">Marketing Portal Description: </label>
		<div class="col-md-5">
			<textarea class="form-control" rows="3" cols="32" type="text" name="mp_description" id="mp_description">
				<?= set_value('mp_description', $job->mp_description) ?>
				
			</textarea>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="mp_search" class="col-md-3 control-label">Portal Search Keywords: </label>
		<div class="col-md-5">
			<textarea class="form-control" rows="3" cols="32" type="text" name="mp_search" id="mp_search">
				<?= set_value('mp_search', $job->mp_search) ?>
			</textarea>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="vehicle_id" class="col-md-3 control-label">Vehicle: </label>
	</div>

	<div class="form-group">
		<div class="input-daterange">
			<label for="release_date" class="col-md-3 control-label">Release Date: </label>
			<div class="col-md-5">
				<input id="release_date" type="text" class="datepicker form-control input-small"name="release_date" value="<?= set_value('release_date', $job->release_date) ?>"/>
			</div>	
		</div>
	</div>
	
	<div class="form-group">
		<label for="notes" class="col-md-3 control-label">Job Notes: </label>
		<div class="col-md-5">
			<textarea class="form-control" rows="3" cols="32" type="text" name="notes" id="notes">
				<?= set_value('notes', $job->notes) ?>
			</textarea>
		</div>	
	</div>

	<div class="col-xs-3 col-sm-1 col-md-4 col-md-offset-4">
		<input class="btn btn-default pull-right" type="submit" value="submit" />
	</div>
</form>