<div style="position:relative;">
    <?php foreach ( $images as $k => $v ): ?>
        <?php $img = $v; ?>
        <div class="annotate-container col-md-6" data-rel="<?= $img_arr[$k] ?>">
            <div class="annotate annotate-tpl" data-rel="<?= $img_arr[$k] ?>">
                <img class="annotate-bubble" src="<?= asset_url() ?>images/bubble.png" />
                <a class="annotate-close"><strong style="color:#ff0000;">x</strong> delete</a>
                <textarea data-rel="<?= $img ?>"></textarea>
            </div>
            <button class="add-comment" data-rel="<?= $img_arr[$k] ?>">Add Comment</button>
            <button class="save-comment" data-rel="<?= $img_arr[$k] ?>">Save</button>
            <img src="<?= $img; ?>" style="display:block;" />
        </div>
    <?php endforeach; ?>
</div>