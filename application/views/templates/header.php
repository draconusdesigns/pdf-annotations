<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
				
<title>Annotations | <?= $titleTag ?></title>

<link rel="stylesheet" type="text/css" href="<?= asset_url() ?>/css/datepicker.css" media="all"/>
<link rel="stylesheet" type="text/css" href="<?= asset_url() ?>/css/themes/flatly.css" media="all"/>
<link rel="stylesheet" type="text/css" href="<?= asset_url() ?>/css/tablesorter.css" media="all"/>
<link rel="stylesheet" type="text/css" href="<?= asset_url() ?>/css/styles.css" media="all"/>
<link rel="stylesheet" type="text/css" href="<?= asset_url() ?>/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?= asset_url() ?>/css/jquery.annotate.css">
<script type="text/javascript">window.base_url = <?php echo json_encode(base_url()); ?>;</script>
<script type="text/javascript" src="<?= asset_url() ?>/js/jquery-1.11.js"></script>
<script type="text/javascript" src="<?= asset_url() ?>/js/jquery-ui.js"></script>

<script type="text/javascript" src="<?= asset_url() ?>/js/bootstrap.js"></script>
<script type="text/javascript" src="<?= asset_url() ?>/js/jquery.tablesorter.js"></script>

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.3/themes/base/jquery-ui.css">
<script type="text/javascript" src="<?= asset_url() ?>/js/jquery.annotate.js"></script>

<script src="<?= asset_url() ?>/js/scripts.js"></script>

<!--[if lt IE 9]>
	<script type="text/javascript" src="<?= asset_url() ?>/js/html5shiv.js"></script>
	<script type="text/javascript" src="<?= asset_url() ?>/js/respond.js"></script>
<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
	  <!-- Brand and toggle get grouped for better mobile display -->
	  <div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="<?= base_url() ?>"><i class="fa fa-home fa-fw"></i> Annotations</a>
	  </div>
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav">
			<li><a href="<?= base_url() ?>/login/"><i class="fa fa-sign-in"></i> Login</a></li>
		</ul>
<!-- 		<ul class="nav navbar-nav navbar-right userinfo"> -->
      	</ul>
	  </div>
	</nav>
	<div id="wrap">
		<div class="row">
			<div class="col-md-8">
				<?php if($this->session->flashdata('error')):?>
					<div class="alert alert-dismissable alert-danger" style="float: right;">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif; ?>
			    <?php if($this->session->flashdata('success')):?>
			        <div class="alert alert-dismissable alert-success" style="float: right;">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<?= $this->session->flashdata('success') ?>
					</div>
			    <?php endif; ?>

				<?php if($this->session->flashdata('notification')):?>
				    <div class="alert alert-dismissable alert-danger" style="float: right;">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<?= $this->session->flashdata('notification') ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div id="content" class="container">
			<div class="page-header">
				<h1><?= $pageHeading ?></h1>
			<?php if(isSet($pageSubHeading)):?>
				<h4><?= $pageSubHeading ?></h4>
			<?php endif;?>	
			</div>