<div class="row">
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>pdfs/upload" type="button" class="btn btn-primary pull-right" role="button">
			<i class='fa fa-plus-circle'></i> Add a New PDF
		</a>
	</div>
</div>
<div class="row">	
	<table class="table table-striped tablesorter">
		<thead class="thead">
			<th>Files</th>
			<th class="center">Number of Pages</th>
			<td class="center">Convert/View Gallery</td>
		</thead>
		<tbody>

		<?php foreach ($files as $file): ?>
			<tr class="">
				<td><?= $file->file ?></td>
				<td class="center"><?= $file->count ?></td>
				<td class="center">
					<?php if($file->method == 'convert'): ?>
						<a data-rel="<?= $file->name ?>" href="<?=base_url()?>pdfs/convert/<?=$file->name?>">Convert</a>
					<?php else: ?>	
						<a data-rel="<?= $file->name ?>" href="<?=base_url()?>pdfs/gallery/<?=$file->name?>">View</a>
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>	