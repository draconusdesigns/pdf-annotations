$(document).ready(function() {
	$("table.tablesorter").tablesorter();

    var anno = $('.annotate-container');
    anno.each(function(){
        var rel = $(this).data('rel');
        $(this).annotate({
            tpl:'.annotate-tpl[data-rel="' + rel + '"]',
            add:'.add-comment[data-rel="' + rel + '"]'
        });
        $('.save-comment[data-rel="' + rel + '"]').click(function(){
            var sdata = $(this).annotate('serialize');
            $.post(
                window.base_url + 'pdfs/save_comments',
                {sdata:sdata},
                function(data) {
                    alert(JSON.stringify(data));
                }, 'json'
            );
        });
    });

    window.jQuery = window.$ = jQuery;
});